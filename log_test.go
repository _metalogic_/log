// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package log

// These tests are too simple.

import (
	"bytes"
	"fmt"
	"os"
	"regexp"
	"strings"
	"testing"
	"time"
)

const (
	Rdate         = `[0-9][0-9][0-9][0-9]/[0-9][0-9]/[0-9][0-9]`
	Rtime         = `[0-9][0-9]:[0-9][0-9]:[0-9][0-9]`
	Rmicroseconds = `\.[0-9][0-9][0-9][0-9][0-9][0-9]`
	Rline         = `(65|67)` // must update if the calls to l.Infof / l.Info below move
	Rlongfile     = `.*/[A-Za-z0-9_\-]+\.go`
	Rshortfile    = `[A-Za-z0-9_\-]+\.go`
	RMessage      = `"level": "(fatal|error|warn|info|debug|trace)", "message": `
)

type tester struct {
	json    bool
	flag    int
	prefix  string
	pattern string // regexp that log output must match; we add ^ and expected_text$ always
}

var tests = []tester{
	// individual pieces: don't bother testing JSON with date/time
	{false, 0, "", ""},
	{true, 0, "", `{ ` + RMessage},
	{false, 0, "XXX", "XXX"},
	{true, 0, "XXX", `{ "prefix": "XXX", ` + RMessage},
	{false, Ldate, "", Rdate + " "},
	{false, Ltime, "", Rtime + " "},
	{false, Ltime | Lmicroseconds, "", Rtime + Rmicroseconds + " "},
	{false, Lmicroseconds, "", Rtime + Rmicroseconds + " "}, // microsec implies time
	{false, Llongfile, "", Rlongfile + ":" + Rline + ": "},
	{true, Llongfile, "", `{ "file": "` + Rlongfile + `", "line": ` + Rline + `, ` + RMessage},
	{false, Lshortfile, "", Rshortfile + ":" + Rline + ": "},
	{false, Llongfile | Lshortfile, "", Rshortfile + ":" + Rline + ": "}, // shortfile overrides longfile
	// everything at once:
	{false, Ldate | Ltime | Lmicroseconds | Llongfile, "XXX", "XXX" + Rdate + " " + Rtime + Rmicroseconds + " " + Rlongfile + ":" + Rline + ": "},
	{false, Ldate | Ltime | Lmicroseconds | Lshortfile, "XXX", "XXX" + Rdate + " " + Rtime + Rmicroseconds + " " + Rshortfile + ":" + Rline + ": "},
}

// Test using Info("hello", 23, "world") or using Infof("hello %d world", 23)
func testInfo(t *testing.T, enableJSON bool, flag int, prefix string, pattern string, useFormat bool) {
	buf := new(bytes.Buffer)
	SetOutput(buf)
	if enableJSON {
		flag = flag | LJSON
	}
	SetFlags(flag)
	SetPrefix(prefix)
	if useFormat {
		Infof("hello %d world", 23)
	} else {
		Info("hello ", 23, " world")
	}
	line := buf.String()

	line = line[0 : len(line)-1]
	if enableJSON {
		pattern = "^" + pattern + `"hello 23 world" }$`
	} else {
		pattern = "^" + pattern + "hello 23 world$"
	}

	matched, err4 := regexp.MatchString(pattern, line)
	if err4 != nil {
		t.Fatal("pattern did not compile:", err4)
	}
	if !matched {
		t.Errorf("log output should match %q is %q", pattern, line)
	}
	SetOutput(os.Stderr)
}

func TestAll(t *testing.T) {
	for _, testcase := range tests {
		testInfo(t, testcase.json, testcase.flag, testcase.prefix, testcase.pattern, false)
		testInfo(t, testcase.json, testcase.flag, testcase.prefix, testcase.pattern, true)
	}
}

func TestOutput(t *testing.T) {
	const testString = "test"
	var b bytes.Buffer
	l := New(&b, "", 0)
	// l.SetFlags(!LJSON)
	l.Info(testString)
	if expect := testString + "\n"; b.String() != expect {
		t.Errorf("log output should match %q is %q", expect, b.String())
	}
}

func TestOutputRace(t *testing.T) {
	var b bytes.Buffer
	l := New(&b, "", 0)
	for i := 0; i < 100; i++ {
		go func() {
			l.SetFlags(0)
		}()
		l.Output(0, "Testing", "")
	}
}

func TestFlagAndPrefixSetting(t *testing.T) {
	var b bytes.Buffer
	l := New(&b, "Test:", LstdFlags)

	f := l.Flags()
	if f != LstdFlags {
		t.Errorf("Flags 1: expected %x got %x", LstdFlags, f)
	}

	l.SetFlags(Ldate | Ltime | Lmicroseconds)
	f = l.Flags()
	if f != Ldate|Ltime|Lmicroseconds {
		t.Errorf("Flags 2: expected %x got %x", Ldate|Ltime|Lmicroseconds, f)
	}

	l.ClearFlag(Lmicroseconds)
	f = l.Flags()
	if f != Ldate|Ltime {
		t.Errorf("Flags 3: expected %x got %x", Ldate|Ltime, f)
	}

	p := l.Prefix()
	if p != "Test:" {
		t.Errorf(`Prefix: expected "Test:" got %q`, p)
	}
	l.SetPrefix("Reality:")
	p = l.Prefix()
	if p != "Reality:" {
		t.Errorf(`Prefix: expected "Reality:" got %q`, p)
	}
	// Verify a log message looks right, with our prefix and microseconds present.
	l.ToggleFlag(Lmicroseconds)
	l.Info("hello")
	pattern := "^Reality:" + Rdate + " " + Rtime + Rmicroseconds + " hello\n"
	matched, err := regexp.Match(pattern, b.Bytes())
	if err != nil {
		t.Fatalf("pattern %q did not compile: %s", pattern, err)
	}
	if !matched {
		t.Error("message did not match pattern")
	}
}

func TestUTCFlag(t *testing.T) {
	var b bytes.Buffer
	l := New(&b, "Test:", LstdFlags)
	// l.ClearFlag(LJSON)
	l.SetFlags(Ldate | Ltime | LUTC)
	// Verify a log message looks right in the right time zone. Quantize to the second only.
	now := time.Now().UTC()
	l.Info("hello")
	want := fmt.Sprintf("Test:%d/%.2d/%.2d %.2d:%.2d:%.2d hello\n",
		now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
	got := b.String()
	if got == want {
		return
	}
	// It's possible we crossed a second boundary between getting now and logging,
	// so add a second and try again. This should very nearly always work.
	now = now.Add(time.Second)
	want = fmt.Sprintf("Test:%d/%.2d/%.2d %.2d:%.2d:%.2d hello\n",
		now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
	if got == want {
		return
	}
	t.Errorf("got %q; want %q", got, want)
}

func TestEmptyPrintCreatesLine(t *testing.T) {
	var b bytes.Buffer
	l := New(&b, "Header:", LstdFlags)
	l.ClearFlag(LJSON)
	l.Info()
	l.Info("non-empty")
	output := b.String()
	if n := strings.Count(output, "Header"); n != 2 {
		t.Errorf("expected 2 headers, got %d", n)
	}
	if n := strings.Count(output, "\n"); n != 2 {
		t.Errorf("expected 2 lines, got %d", n)
	}
}

func BenchmarkItoa(b *testing.B) {
	dst := make([]byte, 0, 64)
	for i := 0; i < b.N; i++ {
		dst = dst[0:0]
		itoa(&dst, 2015, 4)   // year
		itoa(&dst, 1, 2)      // month
		itoa(&dst, 30, 2)     // day
		itoa(&dst, 12, 2)     // hour
		itoa(&dst, 56, 2)     // minute
		itoa(&dst, 0, 2)      // second
		itoa(&dst, 987654, 6) // microsecond
	}
}

func BenchmarkPrintln(b *testing.B) {
	const testString = "test"
	var buf bytes.Buffer
	l := New(&buf, "", LstdFlags)
	for i := 0; i < b.N; i++ {
		buf.Reset()
		l.Info(testString)
	}
}

func BenchmarkPrintlnNoFlags(b *testing.B) {
	const testString = "test"
	var buf bytes.Buffer
	l := New(&buf, "", 0)
	for i := 0; i < b.N; i++ {
		buf.Reset()
		l.Info(testString)
	}
}
