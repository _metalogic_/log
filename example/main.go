package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/log/example/subtest"
)

var (
	colorizeFlg bool
	levelFlg    log.Level
)

func init() {
	flag.BoolVar(&colorizeFlg, "colorize", false, "enable colorized logging")
	flag.Var(&levelFlg, "level", "set log level value to one of Error, Info, Debug or Trace")
}

func main() {

	flag.Parse()

	log.SetColorize(colorizeFlg)

	fmt.Println("** Default log level is", log.GetLevel())

	fmt.Printf("** Log level from flag is %s\n", levelFlg)

	if levelFlg != log.None {
		log.SetLevel(levelFlg)
	}

	fmt.Println("** Calling log.Infof")

	log.Infof("message: %s, %d", "Info, log file!", 10)

	fmt.Println("** Setting log level to LevelError")
	log.SetLevel(log.ErrorLevel)
	fmt.Println("** New log level is", log.GetLevel())

	fmt.Println("** Calling log.Info")

	log.Info("No Info, log file!")

	fmt.Println("** Calling Loggable(log.InfoLevel)")
	if log.Loggable(log.InfoLevel) {
		fmt.Println("log.InfoLevel should not be printed at log.ErrorLevel")
	}

	fmt.Println("** Calling log.Errorf")

	log.Errorf("%s", "Error, log file!")

	fmt.Println("\n** Calling subtest")

	subtest.Foo()

	fmt.Println("\n** Calling log.Debug")
	log.Debug("Debug, log file!")

	fmt.Println("\n** Calling log.Info")
	log.Info("Info again")

	// try a new logger
	logger := log.New(os.Stderr, "new logger ", log.Lshortfile)

	logger.SetColorize(true)

	logger.Error("calling logger with colorize enabled at calledepth ", 2)

	fmt.Println("** Calling log.Fatal")
	log.Fatal("I'm outta here!")
}
