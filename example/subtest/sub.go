package subtest

import (
	"fmt"

	"bitbucket.org/_metalogic_/log"
)

// Foo ...
func Foo() {

	fmt.Println("\n** subtest setting flags to Lshortfile")
	log.SetFlags(log.Lshortfile)

	fmt.Println("\n** subtest calling log.Info")
	log.Info("Sub Info, log file!")

	fmt.Println("\n** subtest calling log.Error")
	log.Error("Sub Error, log file!")

	fmt.Println("\n** subtest setting log level to DebugLevel")
	log.SetLevel(log.DebugLevel)

	fmt.Println("** Calling Loggable(log.InfoLevel)")
	if log.Loggable(log.InfoLevel) {
		fmt.Println("log.InfoLevel should be printed at DebugLevel")
	}

	fmt.Println("** Calling Loggable(log.DebugLevel)")
	if log.Loggable(log.DebugLevel) {
		fmt.Println("log.DebugLevel should be printed at DebugLevel")
	}

	fmt.Println("\n** subtest calling log.Debug")
	log.Debug("Sub Debug")

	fmt.Println("\n** subtest new log level is", log.GetLevel())
	fmt.Println("\n** leaving subtest")
}
